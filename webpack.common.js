const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const path = require('path');
const ManifestPlugin = require('webpack-manifest-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
    template: path.resolve(__dirname, 'index.html'),
    title: 'Template'
});

module.exports = (env) => ({
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
        publicPath: "/"
    },
    module: {
        noParse: /(mapbox-gl)\.js$/,
        rules: [
            {
                include: [
                    path.resolve(__dirname, "src")
                ],
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [htmlPlugin, new ManifestPlugin()],
});
