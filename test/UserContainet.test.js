import React from "react";
import { shallow } from "enzyme";
import { Container } from "../src/UserContainer";

jest.mock("../src/users/UserDashboard", () => "UserDashboard");

describe("<MyComponent />", () => {
    it("renders Loading spinner", () => {
        const match = { params: {} };
        const wrapper = shallow(<Container
            fetchVendingMachines={jest.fn()}
            isFetching
            match={match}
        />);
        expect(wrapper.find("UserDashboard")).toHaveLength(0);
    });

    it("renders three <Foo /> components", () => {
        const match = { params: {} };
        const wrapper = shallow(<Container
            fetchVendingMachines={jest.fn()}
            match={match}
        />);
        expect(wrapper.find("UserDashboard")).toHaveLength(1);
    });
});
