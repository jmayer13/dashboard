import userReducers, { initialState } from "../../src/users/userReducers";
import * as userActions from "../../src/users/userActions";

describe("User reducers", () => {
    it("CREATE_VENDNG_MACHINE_REQUEST", () => {
        const result = userReducers({}, { type: userActions.CREATE_VENDNG_MACHINE_REQUEST });
        expect(result).toEqual({ });
    });
    it("CREATE_VENDNG_MACHINE_SUCCESS", () => {
        const response = {
            data: { data: {} },
        };
        const action = {
            type: userActions.CREATE_VENDNG_MACHINE_SUCCESS,
            response,
        };
        const result = userReducers(initialState, action);
        expect(result.vendingMachines).toContain(response.data.data);
    });
    it("CREATE_VENDNG_MACHINE_FAILURE", () => {
        const error = {};
        const result = userReducers({}, { type: userActions.CREATE_VENDNG_MACHINE_FAILURE, error });

        expect(result).toEqual({ error });
    });

    it("FETCH_VENDNG_MACHINES_REQUEST", () => {
        const result = userReducers({}, { type: userActions.FETCH_VENDNG_MACHINES_REQUEST });
        expect(result).toEqual({ isFetching: true });
    });
    it("FETCH_VENDNG_MACHINES_SUCCESS", () => {
        const response = {
            data: { data: [] },
        };
        const action = {
            type: userActions.FETCH_VENDNG_MACHINES_SUCCESS,
            response,
        };
        const result = userReducers({}, action);
        expect(result).toEqual({ isFetching: false, vendingMachines: response.data.data });
    });
    it("FETCH_VENDNG_MACHINES_FAILURE", () => {
        const error = {};
        const result = userReducers({}, { type: userActions.FETCH_VENDNG_MACHINES_FAILURE, error });
        expect(result).toEqual({ isFetching: false, error });
    });
});
