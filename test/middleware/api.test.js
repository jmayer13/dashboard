import * as axios from "axios";
import api, { CALL_API } from "../../src/middleware/api";

jest.mock("axios", () => jest.fn(() => (Promise.resolve({}))));

describe("API Middleware", () => {
    it("ignore action without the CALL_API symbol", () => {
        const dispatch = jest.fn();
        const getState = jest.fn();
        const next = jest.fn();
        const action = {
            type: "ACTION",
        };

        api({ dispatch, getState })(next)(action);
        expect(next).toBeCalledWith(action);
    });

    it("dispatch success action with response if request succeed", () => {
        const dispatch = jest.fn();
        const getState = () => ({
        });
        const next = jest.fn();
        const action = {
            [CALL_API]: {
                types: ["REQUEST", "SUCCESS", "FAILURE"],
            },
        };
        const response = { data: {} };


        axios.default.mockResolvedValue(response);

        return api({ dispatch, getState })(next)(action).then(() => {
            expect(dispatch).toBeCalledWith(expect.objectContaining({ response, type: "SUCCESS" }));
        });
    });

    it("add query params to url", () => {
        const dispatch = jest.fn();
        const getState = () => ({});
        const next = jest.fn();
        const action = {
            [CALL_API]: {
                types: ["REQUEST", "SUCCESS", "FAILURE"],
                url: "url",
                query: {
                    test: 123,
                },
            },
        };

        return api({ dispatch, getState })(next)(action).then(() => {
            expect(axios).toBeCalledWith(expect.objectContaining({
                url: "url?test=123",
            }));
        });
    });
});
