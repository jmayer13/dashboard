import React from "react";
import { connect } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import styled from "styled-components";
import * as action from "./users/userActions";
import UserDashboard from "./users/UserDashboard";

const Loading = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
`;

export class Container extends React.Component {
    componentDidMount() {
        const { fetchVendingMachines, match } = this.props;
        const { userId } = match.params;
        fetchVendingMachines(userId);
    }

    render() {
        const {
            vendingMachines, isFetching, onCreateVendingMachine, match,
        } = this.props;
        const { userId } = match.params;

        if (isFetching) {
            return (<Loading><CircularProgress /></Loading>);
        }

        return (
            <UserDashboard
                data={vendingMachines}
                onCreateVendingMachine={onCreateVendingMachine}
                userId={userId}
            />
        );
    }
}

const mapStateToProps = state => ({
    ...state.users,
});

const mapDispatchToProps = {
    fetchVendingMachines: action.fetchVendingMachines,
    onCreateVendingMachine: action.onCreateVendingMachine,
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
