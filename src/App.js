import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from "react-router-dom";
import { connect } from "react-redux";
import Root from "./components/Root";
import UserContainer from "./UserContainer";

const App = () => (
    <Root>
        <Router>
            <Switch>
                <Route
                    path="/users/:userId"
                    component={UserContainer}
                />
            </Switch>
        </Router>
    </Root>
);

const mapStateToProps = state => ({
    ...state,
});
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
