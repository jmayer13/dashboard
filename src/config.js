
// TODO: use a CI tool to include variables in build time
const getConfig = () => ({
    apiHost: "https://frontend-test-api.herokuapp.com/api/v1/",
    mapBoxToken: "TOKEN"
});

export default getConfig();
