import axios from "axios";
import qs from "qs";
import env from "../config";

export const CALL_API = Symbol("Call API");

export default function api({ dispatch }) {
    return next => (action) => {
        if (!Object.prototype.hasOwnProperty.call(action, CALL_API)) {
            return next(action);
        }

        const callAPI = action[CALL_API];
        const {
            url, headers, method, data, query, types,
        } = callAPI;

        const config = {
            method,
            baseURL: env.apiHost,
            url: url + qs.stringify(query, { addQueryPrefix: true }),
            data: qs.stringify(data),
            headers: {
                ...headers,
            },
        };

        const [requestType, successType, failureType] = types;
        dispatch({ type: requestType });

        return axios(config).then(
            response => dispatch({ response, type: successType, request: callAPI }),
        ).catch(
            error => dispatch({ error, type: failureType, request: callAPI }),
        );
    };
}
