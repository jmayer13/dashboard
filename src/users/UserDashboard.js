import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Map from "../components/Map";
import Chart from "./Chart";

const useStyles = makeStyles(theme => ({
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: "flex",
        overflow: "auto",
        flexDirection: "column",
    },
}));

export default function Dashboard(props) {
    const classes = useStyles();
    const onAddPoint = (params) => {
        props.onCreateVendingMachine(props.userId, params);
    };

    return (
        <Container maxWidth="lg" className={classes.container}>
            <Grid container spacing={3}>
                <Grid item xs={12} md={6} lg={5}>
                    <Paper className={classes.paper} style={{ height: 250 }}>
                    Vending Machines
                        <Chart number={props.data.length} style={{ height: 180 }} />
                    </Paper>
                </Grid>
                <Grid item xs={12} md={6} lg={7}>
                    <Paper className={classes.paper} style={{ height: 250 }}>
                        Sales
                        <img
                            src="https://3.bp.blogspot.com/-Hu8vvVwvvNo/V6N7-Xm8AnI/AAAAAAAABpc/f6TDTCiR4qwcAPquDP_WJjZA-ucKPUBLACK4B/s1600/basicgraph.png"
                            alt="demo graph"
                            style={{ height: 200 }}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Map data={props.data} onAddPoint={onAddPoint} />
                    </Paper>
                </Grid>
            </Grid>
        </Container>
    );
}
