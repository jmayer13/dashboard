import { CALL_API } from "../middleware/api";

export const CREATE_VENDNG_MACHINE_REQUEST = "CREATE_VENDNG_MACHINE_REQUEST";
export const CREATE_VENDNG_MACHINE_SUCCESS = "CREATE_VENDNG_MACHINE_SUCCESS";
export const CREATE_VENDNG_MACHINE_FAILURE = "CREATE_VENDNG_MACHINE_FAILURE";
export const FETCH_VENDNG_MACHINES_REQUEST = "FETCH_VENDNG_MACHINES_REQUEST";
export const FETCH_VENDNG_MACHINES_SUCCESS = "FETCH_VENDNG_MACHINES_SUCCESS";
export const FETCH_VENDNG_MACHINES_FAILURE = "FETCH_VENDNG_MACHINES_FAILURE";

export const fetchVendingMachines = userId => dispatch => dispatch({
    [CALL_API]: {
        url: `/users/${userId}/vending_machines`,
        method: "GET",
        types: [
            FETCH_VENDNG_MACHINES_REQUEST,
            FETCH_VENDNG_MACHINES_SUCCESS,
            FETCH_VENDNG_MACHINES_FAILURE,
        ],
    },
});

export const onCreateVendingMachine = (userId, vendingMachine) => dispatch => dispatch({
    [CALL_API]: {
        url: `/users/${userId}/vending_machines`,
        method: "POST",
        types: [
            CREATE_VENDNG_MACHINE_REQUEST,
            CREATE_VENDNG_MACHINE_SUCCESS,
            CREATE_VENDNG_MACHINE_FAILURE,
        ],
        requireAuth: true,
        data: {
            vending_machine: {
                ...vendingMachine,
            },
        },
    },
});
