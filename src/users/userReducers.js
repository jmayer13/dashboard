import createReducer from "../utils/createReducer";
import {
    CREATE_VENDNG_MACHINE_REQUEST,
    CREATE_VENDNG_MACHINE_SUCCESS,
    CREATE_VENDNG_MACHINE_FAILURE,
    FETCH_VENDNG_MACHINES_REQUEST,
    FETCH_VENDNG_MACHINES_SUCCESS,
    FETCH_VENDNG_MACHINES_FAILURE,
} from "./userActions";

export const initialState = {
    vendingMachines: [],
    isFetching: false,
    hasErrors: false,
};

const handlers = {
    [FETCH_VENDNG_MACHINES_REQUEST](state) {
        return {
            ...state,
            isFetching: true,
        };
    },
    [FETCH_VENDNG_MACHINES_SUCCESS](state, { response }) {
        return {
            ...state,
            vendingMachines: response.data.data,
            isFetching: false,
        };
    },
    [FETCH_VENDNG_MACHINES_FAILURE](state, { error }) {
        return {
            ...state,
            error,
            isFetching: false,
        };
    },
    [CREATE_VENDNG_MACHINE_REQUEST](state) {
        return { ...state };
    },
    [CREATE_VENDNG_MACHINE_SUCCESS](state, { response }) {
        return {
            ...state,
            vendingMachines: state.vendingMachines.concat(response.data.data),
        };
    },
    [CREATE_VENDNG_MACHINE_FAILURE](state, { error }) {
        return {
            ...state,
            error,
        };
    },
};

export default createReducer(initialState, handlers);
