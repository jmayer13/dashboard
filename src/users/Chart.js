import React from "react";
import ReactMinimalPieChart from "react-minimal-pie-chart";

// TODO: replace ReactMinimalPieChart lib with D3 or SVG
const Chart = ({ number, style }) => (
    <ReactMinimalPieChart
        data={[{
            value: number,
            color: "#228B22",
        }]}
        totalValue={number}
        lineWidth={15}
        label
        labelStyle={{
            fontSize: "33px",
            fontFamily: "sans-serif",
        }}
        labelPosition={0}
        style={style}
    />
);
export default Chart;
