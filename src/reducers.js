import { combineReducers } from "redux";
import users from "./users/userReducers";

const reducers = combineReducers({
    users,
});

export default reducers;
