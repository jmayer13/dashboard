import React, { useState } from "react";
import mapboxgl from "mapbox-gl";
import styled from "styled-components";
import _ from "lodash";
import {getConfig} from "../config"
const Container = styled.div`
    position: relative;
    height: 320px;
    margin: 0;
`;
// TODO: find a way to mock mapboxgl lib
class Map extends React.Component {
    // TODO: replace local data with redux integration
    constructor(props) {
        super(props);
        this.state = { data: [], newData: [] };
    }

    static getDerivedStateFromProps(props, state) {
        if (props.data.length !== state.data.length) {
            const diff = _.differenceWith(props.data, state.data, _.isEqual);
            return {
                data: props.data,
                newData: diff,
            };
        }
        return null;
    }

    componentDidMount() {
        const {
            accessToken, lon, lat, zoomScale, onAddPoint,
        } = this.props;

        mapboxgl.accessToken = accessToken;

        this.map = new mapboxgl.Map({
            container: "map",
            style: "mapbox://styles/mapbox/light-v9",
            center: [lon, lat],
            zoom: [zoomScale],
        });


        this.map.on("load", () => {
            this.state.data.map(point => new mapboxgl.Marker()
                .setLngLat([point.attributes.longitude, point.attributes.latitude])
                .addTo(this.map));

            this.map.on("click", (e) => {
                const markerHeight = 0; const markerRadius = 0; const
                    linearOffset = 0;
                const popupOffsets = {
                    top: [0, 0],
                    "top-left": [0, 0],
                    "top-right": [0, 0],
                    bottom: [0, -markerHeight],
                    "bottom-left": [linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
                    "bottom-right": [-linearOffset, (markerHeight - markerRadius + linearOffset) * -1],
                    left: [markerRadius, (markerHeight - markerRadius) * -1],
                    right: [-markerRadius, (markerHeight - markerRadius) * -1],
                };

                // TODO: replace with custom control
                const button = document.createElement("button");
                button.style = "background-color: white; border: 2px solid black;";
                button.innerHTML = "Add Vending Machine";
                button.onclick = () => {
                    onAddPoint({
                        longitude: e.lngLat.lng,
                        latitude: e.lngLat.lat,
                    });
                };
                new mapboxgl.Popup({ offset: popupOffsets, className: "my-class" })
                    .setLngLat(e.lngLat)
                    .setDOMContent(button)
                    .setMaxWidth("300px")
                    .addTo(this.map);
            });
        });
    }

    componentDidUpdate(prevProps) {
        const currentStyle = this.props.style;
        const previousStyle = prevProps.style;

        if (this.props.style === null) return;

        if (!_.isEqual(previousStyle, currentStyle)) {
            this.map.setStyle(currentStyle);
        }
    }

    render() {
        if (this.map) {
            this.state.newData.map(point => new mapboxgl.Marker()
                .setLngLat([point.attributes.longitude, point.attributes.latitude])
                .addTo(this.map));
        }

        return (
            <Container>
                <div style={{ top: 0, height: 300 }} id="map" />
            </Container>
        );
    }
}

// TODO: replace with dynamic data
const MapContainer = (props) => {
    const [style, setStyle] = useState(0);
    return (
        <Map
            accessToken={getConfig().mapBoxToken}
            lon={139.7}
            lat={35.7}
            zoomScale={8}
            style={style}
            setStyle={setStyle}
            {...props}
        />
    );
};

export default MapContainer;
