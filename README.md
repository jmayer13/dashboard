# DashnoardVending Machines

I have build a basic MVP for a dashboard, with a map, a donut chart and mocked earnings chart.

I did cut some corners but I think it enought to validate an hypothesys.

There some changes I would made taking in account the context. Some factors like number of users, possibility to use realtime data, mobile support migth require to use SSR and websockers to increase the performance and create a better experience.


## Running

- create a account in https://www.mapbox.com/
- replace the variables in /config.js with your own public token

- run
> npm i
> npm run start

- go to http://localhost:8080/users/{USER_ID}
